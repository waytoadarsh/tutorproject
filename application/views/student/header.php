<!--
	Author: W3layouts
	Author URL: http://w3layouts.com
	License: Creative Commons Attribution 3.0 Unported
	License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE html>
<html lang="en">
<!-- Head -->
<head>
<title>Tutorage an Educational Category Bootstrap Responsive Website Template | Gallery :: w3layouts</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta charset="utf-8">
<meta name="keywords" content="Tutorage a Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
<!-- menu -->
<link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>/css/cm-overlay.css" />
<!-- //menu -->
<link href="<?php echo base_url(); ?>/css/jquery.fatNav.css" rel="stylesheet" type="text/css">
<!-- custom css theme files -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>/css/bootstrap.css" type="text/css" media="all">
	<link rel="stylesheet" href="<?php echo base_url(); ?>/css/font-awesome.css" />
	<link rel="stylesheet" href="<?php echo base_url(); ?>/css/style.css" type="text/css" media="all">
<!-- //custom css theme files -->
<link rel="stylesheet" href="<?php echo base_url(); ?>/css/lightbox.css">
<!-- google fonts -->
<link href="//fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i&amp;subset=cyrillic,cyrillic-ext,greek,greek-ext,latin-ext,vietnamese" rel="stylesheet">
<link href="//fonts.googleapis.com/css?family=Montserrat:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i&amp;subset=cyrillic,cyrillic-ext,latin-ext,vietnamese" rel="stylesheet">
<!-- //google fonts -->
</head>
<!-- Body -->
<body>
<!-- banner -->
<header>
	<nav class="w3-navbar py-2">
						<div class="nav-top">
							  <!-- Logo -->
								<div class="logo">
								  <a href="index.html">
									Tutorage
								  </a>
							  </div>
							  <!-- Logo -->
							<div class="right">
								<div class="w3-socials">
										<li><i class="fas fa-phone"></i>
											040-589589
										</li>
								</div>
								
							</div>
							<div class="clearfix"></div>
						</div>
						
					 <!-- end container -->
				
	</nav>
</header>
<div class="w3l-banner-1">
<div class="wthree-dot">
	<!-- nav -->
	<div class="w3layouts-nav-right">
		<div class="fat-nav">
			<div class="fat-nav__wrapper">
				<ul>
					<li><a href="<?php echo site_url(); ?>">Home</a></li>
					<li><a href="<?php echo site_url('student/about'); ?>">About</a></li>
					<li><a href="registration.html">Registration</a></li>
					<li><a href="login.html">Login</a></li>
					
					<li><a href="logout.html">Logout</a></li>
				</ul>
			</div>
		</div>
	</div>		
	<!-- //nav -->
	<!-- //Header -->
	</div>
</div>
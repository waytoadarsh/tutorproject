
<?php
include("header_theme.php");
?>
<br>
<div class="container">
	<form method="post" id="form1">
		<table align="center">
			<h1 style="color: red;" align="center"> TUTOR REGISTRATION </h1><br>
			<tr>
				<td id="cl">NAME</td>
                <td><input type="text" name="name"></td>
                <td>&nbsp;<?php echo form_error("name"); ?></td>
			</tr>
			<tr>
				<td id="cl">GENDER</td>
                <td><input type="radio" name="gender" value="male"><label id="cl">male</label>
                <input type="radio" name="gender" value="female"><label id="cl">female</label></td>
                <td>&nbsp;<?php echo form_error("gender"); ?></td>
			</tr>
			<tr>
				<td id="cl">EMAIL</td>
                <td><input type="text" name="email"></td>
                <td>&nbsp;<?php echo form_error("email"); ?></td>
			</tr>
		
			<tr>
				<td id="cl">contact number</td>
                <td><input type="number" name="phone"></td>
                <td>&nbsp;<?php echo form_error("phone"); ?></td>
			</tr>
			<tr>
				<td id="cl">CLASS</td>
                <td>
                	<select name="class">
                		<option value="" selected="selected" disabled="disabled">class</option>
                		<option value="5">5</option>
                		<option value="6">6</option>
                		<option value="7">7</option>
                		<option value="8">8</option>
                		<option value="9">9</option>
                		<option value="10">10</option>
                	</select>
                        <td>&nbsp;<?php echo form_error("class"); ?></td>
                </td>

			</tr>
			<tr>
				<td id="cl">SUBJECT</td>
                <td>
                	<select name="sub">
                		<option value="" selected="selected" disabled="disabled">subject</option>
                		<option value="maths">Mathematics</option>
                		<option value="science">science</option>
                		<option value="social science">social science</option>
                		<option value="english">English</option>
                		<option value="biology">biology</option>
                		<option value="chemistry">chemistry</option>
                		<option value="physics">physics</option>
                		<option value="geography">geography</option>
                		<option value="history">history</option>
                	</select>

                </td>
			</tr>
			<tr>
				<td id="cl">DISTRICT</td>
                <td>
                	<select name="place">
                		<option value="" selected="selected" disabled="disabled">District</option>
                		<option value="Trivandrum">Trivandrum</option>
                		<option value="Kollam">Kollam</option>
                		<option value="Pathanamthitta">Pathanamthitta</option>
                		<option value="Alappuzha">Alappuzha</option>
                		<option value="Kottayam">Kottayam</option>
                		<option value="Idukki">Idukki</option>
                		<option value="Ernakulam">Ernakulam</option>
                		<option value="Thrissur">Thrissur</option>
                		<option value="Palakad">Palakad</option>
                		<option value="Kozhikode">Kozhikode</option>
                		<option value="Malappuram">Malappuram</option>
                		<option value="Wayanad">Wayanad</option>
                		<option value="Kannur">Kannur</option>
                		<option value="Kasargode">Kasargode</option>

                	</select>
                </td>
                <td>&nbsp;<?php echo form_error("place"); ?></td>
			</tr>
			<tr>
				<td id="cl">Address</td>
                <td><textarea name="address"></textarea></td>
                <td>&nbsp;<?php echo form_error("address"); ?></td>
			</tr>
			<tr>
				<td id="cl">PIN CODE</td>
                <td><input type="number" name="pin"></td>
                <td>&nbsp;<?php echo form_error("pin"); ?></td>
			</tr>
			<tr>
				<td id="cl">PASSWORD</td>
                <td><input type="password" name="pwd" id="pw"></td>
                <td>&nbsp;<?php echo form_error("pwd"); ?></td>
			</tr>
			<tr>
				<td id="cl">CONFIRM PASSWORD</td>
                <td><input type="password" name="cpwd"></td>
                <td>&nbsp;<?php echo form_error("cpwd"); ?></td>
			</tr>
                        <tr>
                                <td id="cl">EXPERIENCE</td>
                <td><input type="number" name="experience"></td>
                <td>&nbsp;<?php echo form_error("experience"); ?></td>
                        </tr>
			
			<tr>
				<td colspan="2" align="center"><input type="submit" name="subm" value="REGISTER"></td>
			</tr>
			<tr>
				<td colspan="3" align="center"  style="color: red;"><?php echo $msg; ?></td>
			</tr>
		</table>
	</form>
</div>
<br>
<?php
include("footer.php");
?>


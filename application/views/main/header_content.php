<div class="container">
			<div class="flexslider-info">
				<section class="slider">
					<div class="flexslider">
						<ul class="slides">
							<li>
							<div class="w3l-info">
								<h1>Start Learning For successful future</h1>
								<p>We have the perfect accommodation for you.</p>
								
							</div>
							</li>
							<li>
								<div class="w3l-info">
								<h2>Making best future popular education</h2>
								<p>We have the perfect accommodation for you.</p>
								
							</div>
							</li>
							<li>
								<div class="w3l-info">
								<h3>Start Learning For successful future</h3>
								<p>We have the perfect accommodation for you.</p>
								
							</div>
							</li>
						</ul>
					</div>
				</section>
			</div>
		</div>
	</div>
</section>
<!-- //banner -->
<!--banner form-->
	<section class="banner_form py-5">
		<div class="container py-md-4 mt-md-3">
		<h3 class="w3ls-title text-uppercase text-center">our education</h3>
			<div class="row ban_form mt-3 pt-md-5">
				<div class="col-lg-8 bg-white fom-left">
				<div class="row">
						<div class="col-md-6 col-sm-6 categories_sub cats">
							<div class="categories_sub1">
								<h3 class="mt-3">Quick Help</h3>
								<p class="mt-3 mb-5">contact number 9645010032 gmail binetmaryvarghese@gmail.com</p>
							</div>
							<div class="categories_sub1">
								<h3 class="mt-3"> Scholarship For Students</h3>
								<p class="mt-3 mb-5">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do</p>
							</div>
							<div class="categories_sub1">
								<h3 class="mt-3">Sports & Events</h3>
								<p class="mt-3 mb-3">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do</p>
							</div>
						</div>
						<div class="col-md-6 col-sm-6 categories_sub cats1">
							<div class="categories_sub2">
								<h3 class="mt-3">In Your Country</h3>
								<p class="mt-3 mb-5">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do</p>
							</div>
							<div class="categories_sub2">
								<h3 class="mt-3">Sports & Events</h3>
								<p class="mt-3 mb-5">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do</p>
							</div>
							<div class="categories_sub2">
								<h3 class="mt-3">Quick Help</h3>
								<p class="mt-3 mb-3">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do</p>
							</div>
						</div>
					</div>
				</div>
				
			</div>
		</div>
	</section>
	<!--//banner form-->
	<!-- slide -->
		<section class="slide-bg py-5">
		<div class="container py-md-4 mt-md-3">
				<div class="bg-pricemain mt-md-3 pt-5">
					<h3 class="agile-title text-uppercase text-white">Get 100 of online courses for free </h3>
					<span class="w3-line"></span>
					<h5 class="agile-title text-capitalize pt-4"> We Have 15 Years Experience In This Passion</h5>
					<p class="text-light py-4">Aliquam ac est vel nisl condimentum interdum vel eget enim. Curabitur mattis orci sed leo mattis, nec maximus nibh faucibus.
						Mauris et justo vel nibh rhoncus venenatis. Nullal condimentum interdum vel eget enim. Curabitur mattis orci sed le.
					</p>
					<a href="about.html" class="text-uppercase serv_link align-self-center bg-light btn px-4">About me</a>
				</div>
			</div>
		</section>
	<!-- //slide -->
	<!-- services -->
	<section class="agileits-services text-center py-5">
		<div class="container py-md-4 mt-md-3">
			<h3 class="w3ls-title text-uppercase">services </h3>
			<!-- service grid row-->
			<div class="agileits-services-row row mt-md-3 pt-5">
				<div class="col-lg-4 col-md-6 mb-5 agileits-services-grids  order-md-1 order-1">
					<span class="fas fa-book"></span>
					<h4 class="mt-2 mb-2">Best Education System</h4>
					<p>Itaque earum rerum hic tenetur a sapiente delectus reiciendis maiores</p>
				</div>
				<div class="col-lg-4 col-md-6 mb-5 agileits-services-grids order-md-2 order-2">
					<span class="fas fa-graduation-cap"></span>
					<h4 class="mt-2 mb-2">Learning Management</h4>
					<p>Itaque earum rerum hic tenetur a sapiente delectus reiciendis maiores</p>
				</div>
				<div class="col-lg-4 col-md-6 mb-5 agileits-services-grids order-md-3 order-3">
					<span class="fas fa-id-card"></span>
					<h4 class="mt-2 mb-2">Online Certification</h4>
					<p>Itaque earum rerum hic tenetur a sapiente delectus reiciendis maiores</p>
				</div>
				<div class="col-lg-4 col-md-6 mb-5 agileits-services-grids  order-md-4 order-4">
					<span class="fas fa-book"></span>
					<h4 class="mt-2 mb-2">Best Education System</h4>
					<p>Itaque earum rerum hic tenetur a sapiente delectus reiciendis maiores</p>
				</div>
				<div class="col-lg-4 col-md-6 md-mb-5 agileits-services-grids order-md-5 order-5">
					<span class="fas fa-graduation-cap"></span>
					<h4 class="mt-2 mb-2">Learning Management</h4>
					<p>Itaque earum rerum hic tenetur a sapiente delectus reiciendis maiores</p>
				</div>
				<div class="col-lg-4 col-md-6 md-mb-5 agileits-services-grids order-md-6 order-6">
					<span class="fas fa-id-card"></span>
					<h4 class="mt-2 mb-2">Online Certification</h4>
					<p>Itaque earum rerum hic tenetur a sapiente delectus reiciendis maiores</p>
				</div>
			</div>
			<!-- // service grid row-->
		</div>
	</section>
<!-- services -->
<!-- services bottom -->
	<section class="serv_bottom py-5">
		<div class="container py-md-4 mt-md-3">
			<h4 class="agile-ser_bot text-capitalize text-white text-center">Subscribe Today For New Updates</h4>
				<div class="newsright mt-md-3 pt-5">
					<form action="#" method="post">
						<input type="email" placeholder="Enter your email..." name="email" required="">
						<input type="submit" value="Subscribe">
					</form>
				</div>
		</div>
	</section>
<!-- //services bottom -->
<!-- promotions -->
	<section class="wthree-row w3-about  py-5">
		<div class="container py-md-4 mt-md-3">
			<h3 class="w3ls-title text-uppercase text-center">Popular Courses</h3>
			<div class="card-deck mt-md-3 pt-5">
				  <div class="card">
					<img src="<?php echo base_url(); ?>/images/g1.jpg" class="img-fluid" alt="Card image cap">
					<div class="card-body w3ls-card">
					  <h5 class="card-title">Learn Photoshop.</h5>
					  <p class="card-text mb-3 ">This is a longer card with supporting text below as a natural lead-in to additional content.</p>
					</div>
				  </div>
				  <div class="card">
					<img src="<?php echo base_url(); ?>/images/g2.jpg" class="img-fluid" alt="Card image cap">
					<div class="card-body w3ls-card">
					  <h5 class="card-title">Learn HTML5.</h5>
					   <p class="card-text mb-3 ">This is a longer card with supporting text below as a natural lead-in to additional content.</p>
					</div>
				  </div>
				  <div class="card">
					<img src="<?php echo base_url(); ?>/images/g3.jpg" class="img-fluid" alt="Card image cap">
					<div class="card-body w3ls-card">
					  <h5 class="card-title">Learn Node.js.</h5>
					   <p class="card-text mb-3 ">This is a longer card with supporting text below as a natural lead-in to additional content.</p>
					</div>
				  </div>
				</div>
            </div>
    </section>
<!-- //promotions -->
<!-- footer -->
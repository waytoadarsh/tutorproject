
<?php
include("header_theme.php");
?>
<br>
<div class="container">
	<form method="post" id="form1">
		<table align="center">
			<h1 style="color: red;" align="center"> STUDENT REGISTRATION </h1><br>
	        <tr>
		<td id="cl">NAME</td>
                <td><input type="text" name="name"></td>
                <td>&nbsp;<?php echo form_error("name"); ?></td>
			</tr>
			<tr>
				<td id="cl">GENDER</td>
                <td><input type="radio" name="gender" value="male"><label id="cl">male</label>
                <input type="radio" name="gender" value="female"><label id="cl">female</label></td>
                <td>&nbsp;<?php echo form_error("gender"); ?></td>
			</tr>
			<tr>
				<td id="cl">EMAIL</td>
                <td><input type="text" name="email"></td>
                <td>&nbsp;<?php echo form_error("email"); ?></td>
			</tr>
		<!--	<tr>
				<td id="cl">profile picture</td>
                <td><input type="file" name="pic" value="browse"></td>
                <td>&nbsp;<?php echo form_error("pic"); ?></td>
			</tr>-->
			<tr>
				<td id="cl">contact number</td>
                <td><input type="number" name="phno"></td>
                <td>&nbsp;<?php echo form_error("phno"); ?></td>
			</tr>
			<tr>
				<td id="cl">CLASS</td>
                <td>
                	<select name="class">
                		<option value="" selected="selected" disabled="disabled">class</option>
                		<option value="5">5</option>
                		<option value="6">6</option>
                		<option value="7">7</option>
                		<option value="8">8</option>
                		<option value="9">9</option>
                		<option value="10">10</option>
                	</select>
                        <td>&nbsp;<?php echo form_error("class"); ?></td>
                </td>

			</tr>
			<tr>
				<td id="cl">SUBJECT</td>
                <td>
                	<select name="sub">
                		<option value="" selected="selected" disabled="disabled">subject</option>
                		<option value="maths">Mathematics</option>
                		<option value="sci">science</option>
                		<option value="ss">social science</option>
                		<option value="eng">English</option>
                		<option value="biology">biology</option>
                		<option value="che">chemistry</option>
                		<option value="phy">physics</option>
                		<option value="geo">geography</option>
                		<option value="his">history</option>
                	</select>

                </td>
			</tr>
			<tr>
				<td id="cl">DISTRICT</td>
                <td>
                	<select name="district">
                		<option value="" selected="selected" disabled="disabled">District</option>
                		<option value="tvm">Trivandrum</option>
                		<option value="klm">Kollam</option>
                		<option value="ptm">Pathanamthitta</option>
                		<option value="alp">Alappuzha</option>
                		<option value="ktm">Kottayam</option>
                		<option value="idk">Idukki</option>
                		<option value="ekm">Ernakulam</option>
                		<option value="thr">Thrissur</option>
                		<option value="plk">Palakad</option>
                		<option value="kzd">Kozhikode</option>
                		<option value="mlp">Malappuram</option>
                		<option value="wyd">Wayanad</option>
                		<option value="knr">Kannur</option>
                		<option value="ksd">Kasargode</option>

                	</select>
                </td>
                <td>&nbsp;<?php echo form_error("district"); ?></td>
			</tr>
			<tr>
				<td id="cl">Address</td>
                <td><textarea name="addr"></textarea></td>
                <td>&nbsp;<?php echo form_error("addr"); ?></td>
			</tr>
			<tr>
				<td id="cl">PIN CODE</td>
                <td><input type="number" name="pin"></td>
                <td>&nbsp;<?php echo form_error("pin"); ?></td>
			</tr>
			<tr>
				<td id="cl">PASSWORD</td>
                <td><input type="password" name="pwd" id="pw"></td>
                <td>&nbsp;<?php echo form_error("pwd"); ?></td>
			</tr>
			<tr>
				<td id="cl">CONFIRM PASSWORD</td>
                <td><input type="password" name="cpwd"></td>
                <td>&nbsp;<?php echo form_error("cpwd"); ?></td>
			</tr>
			
			<tr>
				<td colspan="2" align="center"><input type="submit" name="subm" value="REGISTER"></td>
			</tr>
			<tr>
				<td colspan="3" align="center"  style="color: red;"><?php echo $msg; ?></td>
			</tr>
		</table>
	</form>
</div>
<br>
<?php
include("footer.php");
?>


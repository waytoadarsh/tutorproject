
<section class="footer footer_w3layouts_section_1its py-md-5">
	<div class="container py-5">
		<div class="row footer-top">
			<div class="col-lg-3 footer-grid_section_1its_w3">
				<div class="footer-title">
					<h3>About Us</h3>
				</div>
				<div class="footer-text">
					<p>Curabitur non nulla sit amet nislinit tempus convallis quis ac lectus. lac inia eget consectetur sed, convallis at tellus.
						Nulla porttitor accumsana tincidunt. Vestibulum ante ipsum primis tempus convallis.</p>
					<ul class="social_section_1info">
						<li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
						<li><a href="#"><i class="fab fa-twitter"></i></a></li>
						<li><a href="#"><i class="fab fa-google-plus-g"></i></i></a></li>
						<li><a href="#"><i class="fab fa-linkedin-in"></i></i></a></li>
					</ul>
				</div>
			</div>
			<div class="col-lg-3 footer-grid_section_1its_w3">
				<div class="footer-title">
					<h3>Contact Info</h3>
				</div>
				<div class="contact-info">
					<h4>Location :</h4>
					<p>0926k 4th block building, king Avenue, New York City.</p>
					<div class="phone">
						<h4>Phone :</h4>
						<p>Phone : +121 098 8907 9987</p>
						<p>Email : <a href="mailto:info@example.com">info@example.com</a></p>
					</div>
				</div>
			</div>
			<div class="col-lg-2 footer-grid_section_1its_w3">
				<div class="footer-title">
					<h3>Useful Links</h3>
				</div>
				<ul class="links">
					<li><a href="index.html">Home</a></li>
					<li><a href="about.html">About</a></li>
					<li><a href="registration.html">Registration</a></li>
					<li><a href="gallery.html">Gallery</a></li>
					<li><a href="contact.html">Contact Us</a></li>
				</ul>
			</div>
			<div class="col-lg-4 footer-grid_section_1its_w3">
				<div class="footer-title">
					<h3>Latest News</h3>
				</div>
					<div class="d-flex justify-content-around">
						<a href="#" class="col-4 fot_tp p-2">
							<img src="<?php echo base_url(); ?>/images/g1.jpg" class="img-fluid" alt="Responsive image">
						</a>
						<a href="#" class="col-4 fot_tp p-2">
							<img src="<?php echo base_url(); ?>/images/g2.jpg" class="img-fluid" alt="Responsive image">
						</a>
						<a href="#" class="col-4 fot_tp p-2">
							<img src="<?php echo base_url(); ?>/images/g3.jpg" class="img-fluid" alt="Responsive image">
						</a>
					</div>
					<div class="d-flex justify-content-around">
						<a href="#" class="col-4 fot_tp p-2">
							<img src="<?php echo base_url(); ?>/images/g4.jpg" class="img-fluid" alt="Responsive image">
						</a>
						<a href="#" class="col-4 fot_tp p-2">
							<img src="<?php echo base_url(); ?>/images/g5.jpg" class="img-fluid" alt="Responsive image">
						</a>
						<a href="#" class="col-4 fot_tp p-2">
							<img src="<?php echo base_url(); ?>/images/g6.jpg" class="img-fluid" alt="Responsive image">
						</a>
					</div>
			</div>
		</div>-->
		<!--<div class="copyright">
			<p>© 2018 Tutorage. All Rights Reserved | Design by <a href="http://w3layouts.com/">W3layouts</a> </p>
		</div>
	</div>
</section>
</footer>-->

<!-- Default-JavaScript-File -->
	<script type="text/javascript" src="<?php echo base_url(); ?>/js/jquery-2.2.3.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>/js/bootstrap.js"></script>
<!-- //Default-JavaScript-File -->
<!--menu script-->
<script type="text/javascript" src="<?php echo base_url(); ?>/js/modernizr-2.6.2.min.js"></script>
		<script src="<?php echo base_url(); ?>/js/jquery.fatNav.js"></script>
		<script>
		(function() {
			
			$.fatNav();
			
		}());
		</script>
<!--Start-slider-script-->
	<script defer src="<?php echo base_url(); ?>/js/jquery.flexslider.js"></script>
		<script type="text/javascript">
		
		$(window).load(function(){
		  $('.flexslider').flexslider({
			animation: "slide",
			start: function(slider){
			  $('body').removeClass('loading');
			}
		  });
		});
	  </script>
<!--End-slider-script-->
</body>
<!-- //Body -->
</html>
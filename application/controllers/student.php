<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class student extends CI_Controller {

	public function registration()
	{
		
		$this->load->library("form_validation");
		$this->form_validation->set_rules("name","student name","required");
		$this->form_validation->set_rules("gender","gender","required");
		$this->form_validation->set_rules("email","email","required");
		$this->form_validation->set_rules("pic","profile pic","required");
		
		$this->form_validation->set_rules("phno","contact number","required");
		$this->form_validation->set_rules("class","class","required");
		$this->form_validation->set_rules("sub","subject","required");
		$this->form_validation->set_rules("district","district","required");
		$this->form_validation->set_rules("addr","address","required");
		$this->form_validation->set_rules("pin","pin code","required");
		$this->form_validation->set_rules("pwd","password","required");
		$this->form_validation->set_rules("cpwd","confirm password","required");
		
		$data['msg']='please fill the application';
		if($this->form_validation->run())
		{
			$this->load->model("StudentModel");
			$data['msg']=$this->StudentModel->insertstudent($_POST);
			
		}
		$this->load->view('student/std_reg',$data);
	}
	
}

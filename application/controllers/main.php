<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class main extends CI_Controller {

	public function index()
	{
	
		$this->load->view('main/index');

	}
	public function searchtutor()
	{
		$this->load->library("form_validation");
		$this->form_validation->set_rules("class","class","required");
		$this->form_validation->set_rules("sub","Subject","required");
		$this->form_validation->set_rules("pin","Pin Code","required");
		$passArr=array();
		if($this->form_validation->run())
		{
			
			$this->load->model("StudentModel");
			$resobj = $this->StudentModel->gettutor($_POST['class'],$_POST['sub'],$_POST['pin']);
			if($resobj->num_rows())
			{
				$passArr["tutor"] = $resobj->result();
			}
			else
			{
				$passArr["msg"]="No tutor found";
			}
			/*
			



			if($resobj->num_rows())
			{
				$passArr["tutor"] = $resobj->result();
			}
			else
			{
				$passArr["msg"]="No tutor found";
			}
		
		
		*/
			}
		$this->load->view('main/search_tutor',$passArr);

	}

	public function viewtutor()
	{
		$this->load->library("form_validation");
		$this->form_validation->set_rules("class","class","required");
		$this->form_validation->set_rules("sub","subject","required");
		$this->form_validation->set_rules("pin","pincode","required");
		$passArr=array();
		if($this->form_validation->run())
		{
			
			$this->db->where(array("class"=>$_POST['class'],"sub"=>$_POST['sub'],"pin"=>$_POST['pin']));
			$resobj=$this->db->get('tutor');



			if($resobj->num_rows())
			{
				$passArr["tutor"] = $resobj->result();
			}
			else
			{
				$passArr["msg"]="No tutor found";
			}
		}
		$this->load->view('student/viewtutor',$passArr);
	}
	
	public function registration()
	{
		
		$this->load->library("form_validation");
		$this->form_validation->set_rules("name","student name","required");
		$this->form_validation->set_rules("gender","gender","required");
		$this->form_validation->set_rules("email","email","required");
		//$this->form_validation->set_rules("pic","profile pic","required");
		
		$this->form_validation->set_rules("phno","contact number","required");
		$this->form_validation->set_rules("class","class","required");
		$this->form_validation->set_rules("sub","subject","required");
		$this->form_validation->set_rules("district","district","required");
		$this->form_validation->set_rules("addr","address","required");
		$this->form_validation->set_rules("pin","pin code","required");
		$this->form_validation->set_rules("pwd","password","required");
		$this->form_validation->set_rules("cpwd","confirm password","required");
		
		$data['msg']='please fill the application';
		if($this->form_validation->run())
		{
			$this->load->model("StudentModel");
			$data['msg']=$this->StudentModel->insertstudent($_POST);
			
		}
		$this->load->view('main/registration',$data);
	}
	public function tutoreg()
	{
		
		$this->load->library("form_validation");
		$this->form_validation->set_rules("name","tutor name","required");
		$this->form_validation->set_rules("gender","gender","required");
		$this->form_validation->set_rules("email","email","required");
		
		$this->form_validation->set_rules("phone","phone","required");
		$this->form_validation->set_rules("class","class","required");
		$this->form_validation->set_rules("sub","subject","required");
		$this->form_validation->set_rules("place","place","required");
		$this->form_validation->set_rules("address","address","required");
		$this->form_validation->set_rules("pin","pin code","required");
		$this->form_validation->set_rules("pwd","password","required");
		$this->form_validation->set_rules("cpwd","confirm password","required");
		$this->form_validation->set_rules("experience","experience","required");
		$data['msg']='please fill the application';
		if($this->form_validation->run())
		{
			$this->load->model("StudentModel");
			$data['msg']=$this->StudentModel->inserttutor($_POST);
			
		}

		$this->load->view('main/tutoreg',$data);
	}
	public function login()
	{
		$this->load->library("form_validation");
		$this->form_validation->set_rules("email","email","required");
		$this->form_validation->set_rules("pwd","password","required");
		$data=array();
		if($this->form_validation->run())
		{
			if($_POST['email']=="admin")
			{
				//echo $_POST["pwd"];
				$this->db->select('pwd');
				$this->db->where('uname',$_POST['email']);
				$resobj=$this->db->get('admin');
				if($resobj->num_rows())
				{
					$row= $resobj->row();
					//echo $row->pwd;
					if($row->pwd == $_POST["pwd"])
					{
						
					redirect("admin/index");
						
						
					}
					else
					{
						$data["msg"] = "Invalid Username or password";
					}
				}
				else
				{
					$data["msg"] = "Invalid Username or password";
				}
			}
			else
			{
				$this->db->select('pwd');
				$this->db->where('email',$_POST['email']);
				$resobj=$this->db->get('student');
				if($resobj->num_rows())
				{
					$row= $resobj->row();
					
					if($row->pwd == $_POST["pwd"])
					{
						
							$this->session->set_userdata("student",$_POST["username"]);
							redirect("main/searchtutor");
						
						
					}
					else
					{
						$data["msg"] = "Invalid Username or password";
					}
				}
				else
				{
					$data["msg"] = "Invalid Username or password";
				}
		}
			
		}
		$this->load->view('main/login',$data);
	}


	public function logout()
	{
		
		$this->session->sess_destroy();
		unset($_SESSION['username']);
		$this->session->unset_userdata('username');
		redirect("main/index");
	}
	
	
}

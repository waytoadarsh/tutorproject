-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: May 29, 2019 at 07:31 AM
-- Server version: 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `gifi_project`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE IF NOT EXISTS `admin` (
  `id` int(50) NOT NULL AUTO_INCREMENT,
  `uname` varchar(50) NOT NULL,
  `pwd` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `uname`, `pwd`) VALUES
(1, 'admin', 'admin123');

-- --------------------------------------------------------

--
-- Table structure for table `login`
--

CREATE TABLE IF NOT EXISTS `login` (
  `id` int(50) NOT NULL AUTO_INCREMENT,
  `email` varchar(50) NOT NULL,
  `pwd` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=17 ;

--
-- Dumping data for table `login`
--

INSERT INTO `login` (`id`, `email`, `pwd`) VALUES
(5, 'george@gmail.com', 'george'),
(6, 'mamatha@gmail.com', 'mamu'),
(7, 'mamatha@gmail.com', 'mamu'),
(8, 'mamatha@gmail.com', 'mamu'),
(9, 'mamatha@gmail.com', 'mamu'),
(10, 'sneha@gmail.com', 'sneha'),
(11, 'sunitha@gmail.com', 'sunitha'),
(12, 'manju@gmail.com', 'manju'),
(13, 'athira@gmail.com', 'athira'),
(14, 'dhanya@gmail.com', 'dhanya'),
(15, 'ila@gmail.com', 'ila'),
(16, 'priyanka@gmail.com', 'priyanka');

-- --------------------------------------------------------

--
-- Table structure for table `student`
--

CREATE TABLE IF NOT EXISTS `student` (
  `id` int(50) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `gender` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `phno` int(50) NOT NULL,
  `class` varchar(50) NOT NULL,
  `sub` varchar(50) NOT NULL,
  `district` varchar(50) NOT NULL,
  `addr` varchar(50) NOT NULL,
  `pin` int(50) NOT NULL,
  `pwd` varchar(50) NOT NULL,
  `cpwd` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=20 ;

--
-- Dumping data for table `student`
--

INSERT INTO `student` (`id`, `name`, `gender`, `email`, `phno`, `class`, `sub`, `district`, `addr`, `pin`, `pwd`, `cpwd`) VALUES
(2, 'vandana', 'female', 'reshmamariamjohnk@gmail.com', 2147483647, 'btech', 'REGISTER', 'plk', 'dfth vbhbnj', 686654, 'vandana', 'vandana'),
(3, 'binet mary varghese', 'female', 'binetmaryvarghese@gmail.com', 2147483647, 'btech', 'REGISTER', 'ktm', 'vakacheril house\r\nettumanoor', 686631, 'binet', 'binet'),
(4, 'GIFI GEORGE', 'female', 'gifigeorge1995@gmail.com', 2147483647, 'btech', 'REGISTER', 'ekm', 'thadathil house\r\npaduvapuram', 683576, 'hellohai', 'hellohai'),
(5, 'britty', 'female', 'britty@gmail.com', 2147483647, '10', 'physics', 'ptm', 'vakacheriyil house\r\npathanapuram', 526485, 'britty', 'britty'),
(12, 'george  t c', 'male', 'george@gmail.com', 2147483647, '6', 'ss', 'idk', 'thadathil house', 683576, 'george', 'george'),
(16, 'ATHIRA A S', 'female', 'athira@gmail.com', 2147483647, '8', 'sci', 'ekm', 'nanadanam\r\nernakulam', 683003, 'athira', 'athira'),
(17, 'dhanya', 'female', 'dhanya@gmail.com', 2147483647, '8', 'biology', 'ekm', 'mattathil\r\npermbavoor', 698745, 'dhanya', 'dhanya'),
(18, 'ILA HARI', 'female', 'ila@gmail.com', 2147483647, '8', 'maths', 'ekm', 'pannathu house\r\ntripunithura', 689574, 'ila', 'ila'),
(19, 'PRIYANKA J', 'female', 'priyanka@gmail.com', 2147483647, '9', 'maths', 'ekm', 'pannattu house\r\nparavoor\r\nernakulam', 68795, 'priyanka', 'priyanka');

-- --------------------------------------------------------

--
-- Table structure for table `tlist`
--

CREATE TABLE IF NOT EXISTS `tlist` (
  `id` int(6) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `class` varchar(50) NOT NULL,
  `sub` varchar(50) NOT NULL,
  `place` varchar(50) NOT NULL,
  `pin` int(20) NOT NULL,
  `phone` int(10) NOT NULL,
  `experience` varchar(30) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `tlist`
--

INSERT INTO `tlist` (`id`, `name`, `class`, `sub`, `place`, `pin`, `phone`, `experience`) VALUES
(1, 'Anu', '10', 'chemistry', 'palakkad', 678544, 123456789, '2yrs'),
(2, 'chinnu', 'btech', 'cs', 'kottayam', 686631, 1234567890, '1 yr');

-- --------------------------------------------------------

--
-- Table structure for table `tutor`
--

CREATE TABLE IF NOT EXISTS `tutor` (
  `id` int(6) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `gender` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `phone` int(20) NOT NULL,
  `class` varchar(10) NOT NULL,
  `sub` varchar(30) NOT NULL,
  `place` varchar(50) NOT NULL,
  `address` varchar(50) NOT NULL,
  `pin` int(10) NOT NULL,
  `experience` varchar(50) NOT NULL,
  `pwd` varchar(30) NOT NULL,
  `cpwd` varchar(30) NOT NULL,
  `status` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `tutor`
--

INSERT INTO `tutor` (`id`, `name`, `gender`, `email`, `phone`, `class`, `sub`, `place`, `address`, `pin`, `experience`, `pwd`, `cpwd`, `status`) VALUES
(1, 'MAMATHA HARIKUMAR', 'female', 'mamatha@gmail.com', 2147483647, '10', 'maths', 'kurichithanam', 'pannathu house', 686639, '10', 'mamu', 'mamu', 'approve'),
(2, 'SNEHA', 'female', 'sneha@gmail.com', 2147483647, '8', 'geo', 'plk', 'kunnathu house\r\npalakkadu', 685478, '', 'sneha', 'sneha', 'approve'),
(3, 'SUNITHA', 'female', 'sunitha@gmail.com', 2147483647, '9', 'english', 'ekm', 'pandarakulam\r\nernakulam', 685974, '8', 'sunitha', 'sunitha', 'approve'),
(4, 'MANJU', 'female', 'manju@gmail.com', 2147483647, '7', 'history', 'idk', 'puthusseriyil\r\nidukki', 686666, '9', 'manju', 'manju', 'approve'),
(5, 'SETHUDHANYA', 'female', 'sethu@gmail.com', 789456123, '10', 'physics', 'Alappuzha', 'varikasseri illam\r\nalla[uzha', 683333, '7', 'sethu', 'sethu', 'remove'),
(6, 'RENJITH', 'male', 'renjith@gmail.com', 123456789, '9', 'biology', 'Wayanad', 'thadathil\r\nwayanad', 681111, '4', 'renjith', 'renjith', 'remove'),
(7, 'Thangamani', 'male', 'thangu@gmail.com', 1234567890, '10', 'science', 'Kasargode', 'wffr ryuuy gnhk', 123456, '3', 'thamgu', 'thangu', 'remove'),
(8, 'aswathy', 'female', 'aswathy@gmail.com', 2147483647, '8', 'social science', 'Wayanad', 'jagadheesh bahavanam\r\nwayanad', 685974, '4', 'aswathy', 'aswathy', ''),
(9, 'adarsh', 'male', 'adarsh@gmail.com', 2147483647, '9', 'english', 'Ernakulam', 'kunnath house', 689574, '7', 'adarsh', 'adarsh', 'remove'),
(10, 'SEETHU GEORGE', 'female', 'seethu@gmail.com', 2147483647, '10', 'english', 'Thrissur', 'karighachirayil house\r\nthrissur', 685974, '2', 'seethu', 'seethu', 'approve'),
(11, 'PRINCY GEORGE', 'female', 'princy@gmail.com', 2147483647, '10', 'maths', 'Kottayam', 'vakacheril house\r\ntheghana', 689754, '3', 'princy', 'princy', 'remove'),
(12, 'reshma', 'female', 'reshmamariamjohnk@gmail.com', 2147483647, '6', 'science', 'Ernakulam', 'ede', 443322, '4', 'ee', 'ee', 'approve');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
